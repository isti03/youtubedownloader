#!/bin/bash

# Copy de files to theirs respective folders
mkdir /home/$USER/.local/share/applications/
mkdir /home/$USER/YouTubeDownloader/
cp YouTubeDownloader.png /home/$USER/.local/share/icons/
cp YouTubeDownloader.desktop /home/$USER/.local/share/applications/
cp Readme.md /home/$USER/YouTubeDownloader/
cp YouTubeDownloader.sh /home/$USER/YouTubeDownloader/
exit
