#!/bin/bash

####################################################################################################
# Application for download video/audio/playlist/subs from differents  webs                         #
# To know all the  webs availabe use                                                               #
# youtube-dl --list-extractors                                                                     #
#                                                                                                  #
# Install youtube-dl:                                                                              #
#	sudo wget https://yt-dl.org/downloads/latest/youtube-dl -O /usr/local/bin/youtube-dl           #
#	sudo chmod a+rx /usr/local/bin/youtube-dl                                                      #
#                                                                                                  #
# Version of youtube-dl:                                                                           #
#   youtube-dl --version                                                                           #
# Update to 17/03/2020                                                                             #
#   sudo youtube-dl -U                                                                             #
#                                                                                                  #
# ffmpeg needed, most of linux distribucions already have it install, if not, use:                 #
# - For Debian/Ubuntu and derivatives                                                              #
#       1--> sudo apt update && sudo apt full-upgrade &&  sudo apt install ffmpeg                  #
# - For Arch and derivatives                                                                       #    
#       2--> sudo pacman -Syu && sudo pacman -S ffmpeg                                             #
#                                                                                                  #
#                                                                                                  #
####################################################################################################
funcMenu() {
option=$(kdialog --menu " Select an Option to Download " a " Download Video " b " Download Audio from Video " c " Download Subtitles " d " Download Playlist "  )
if [ "$option" = "a" ]
    then
        funcVideo
elif [ "$option" = "b" ]
    then
        funcAudio
elif [ "$option" = "c" ]
    then
        funcSubs
elif [ "$option" = "d" ]
    then
        funcPlaylist
       
fi
}

funcVideo() {
url=$(kdialog --title " YouTubeDownloader " --inputbox " Choose an URL to Download " "URL" --geometry 700x100)
path=$(kdialog --getexistingdirectory)
cd "$path" || exit
video=$(kdialog --menu " Select a Quality Option" a " 8K --> 4320p " b " 4K --> 2160p " c " 2K --> 1440p " d " FHD --> 1080p " e " HD --> 720p " f " SD --> 480p " g " Choose Quality " 0 "" 1 " For all video resolutions it will choose the best audio quality available " 2 " If the resolutions choosen isn't available it will choose the best resolution available " --geometry 850x250) 


if [ "$video" = "a" ]
    then
        konsole --qwindowgeometry 700x200 -e youtube-dl -f bestvideo[height=4320]+bestaudio/best "$url"
        kdialog --msgbox " Download Complete "
        funcOtherVideo

elif [ "$video" = "b" ]
    then
        konsole --qwindowgeometry 700x200 -e youtube-dl -f bestvideo[height=2160]+bestaudio/best "$url" 
        kdialog --msgbox " Download Complete "
        funcOtherVideo

elif [ "$video" = "c" ]
    then
        konsole --qwindowgeometry 700x200 -e youtube-dl -f bestvideo[height=1440]+bestaudio/best "$url" 
        kdialog --msgbox " Download Complete "
        funcOtherVideo
        
elif [ "$video" = "d" ]
    then
        konsole --qwindowgeometry 700x200 -e youtube-dl -f bestvideo[height=1080]+bestaudio/best "$url" 
        kdialog --msgbox " Download Complete "
        funcOtherVideo

elif [ "$video" = "e" ]
    then
        konsole --qwindowgeometry 700x200 -e youtube-dl -f bestvideo[height=720]+bestaudio/best "$url" 
        kdialog --msgbox " Download Complete "
        funcOtherVideo
        
elif [ "$video" = "f" ]
    then
        konsole --qwindowgeometry 700x200 -e youtube-dl -f bestvideo[height=480]+bestaudio/best "$url" 
        kdialog --msgbox " Download Complete "
        funcOtherVideo
        
elif [ "$video" = "g" ]
    then
        kdialog --msgbox "$(youtube-dl --list-formats "$url")" 
        sleep 3 
        Aformat=$(kdialog --title " YouTubeDownloader " --inputbox " Choose Format Number to Download ")
        konsole --qwindowgeometry 700x200 youtube-dl -f "$Aformat" "$url" 
        kdialog --msgbox " Download Complete "
fi

}

funcAudio() {
url=$(kdialog --title " YouTubeDownloader " --inputbox " Choose an URL to Download" "URL" --geometry 700x100)
path=$(kdialog --getexistingdirectory)
cd "$path" || exit
audio=$(kdialog --menu " Select an Option " a " Best Quality Mp3 " b " Choose Quality/Bitrate " c " Choose between Availables Formats " d " Exit Application " --geometry 350x200)
if [ "$audio" = "a" ]
    then
        konsole --qwindowgeometry 700x200 -e youtube-dl --extract-audio --audio-quality 0 --audio-format mp3 "$url" 
        kdialog --msgbox " Download Complete "
        funcOtherAudio
        
elif [ "$audio" = "b" ]
    then
        abr=$(kdialog --title " Choose Audio Bitrate " --inputbox " Choose Bitrate between 0-9 ")
        konsole --qwindowgeometry 700x200 -e youtube-dl --extract-audio --audio-quality "$abr" --audio-format mp3 "$url" 
        kdialog --msgbox " Download Complete "
        funcOtherAudio
        
elif [ "$audio" = "c" ]
    then
        kdialog --msgbox "$(youtube-dl --list-formats "$url")" 
        sleep 3 
        Aformat=$(kdialog --title " YouTubeDownloader " --inputbox " Choose Format Number to Download ")
        konsole --qwindowgeometry 700x200 -e youtube-dl -f "$Aformat" "$url" 
        kdialog --msgbox " Download Complete "
        funcOtherAudio
         
elif [ "$audio" = "d" ]
    then
        exit
fi
}

funcSubs() {
url=$(kdialog --title " YouTubeDownloader " --inputbox  " Choose an URL to Download " "URL" --geometry 700x100)
path=$(kdialog --getexistingdirectory)
cd "$path" || exit
subs=$(kdialog --menu " Select an Option " a " Download Original Subtitles " b " Download Subtitles by format/language " c " Download All Subtitles " d " Exit Application " --geometry 400x200)
if [ "$subs" = "a" ]
    then
        konsole --qwindowgeometry 700x200 -e youtube-dl --write-sub --skip-download "$url" 
        kdialog --msgbox " Download Complete "
        funcOtherSub
elif [ "$subs" = "b" ]
    then
        sformat=$(kdialog --title " YouTubeDownloader " --inputbox " Choose Subtitle Format \ ass    srt ")
        slang=$(kdialog --title " YouTubeDownloader " --inputbox " Choose Subtitle language \ en  esp  pt  ita  ger ... ")
        konsole --qwindowgeometry 700x200 -e youtube-dl --sub-format "$sformat" --sub-lang "$slang" --skip-download "$url" 
        kdialog --msgbox " Download Complete "
        funcOtherSub
elif [ "$subs" = "c" ]
    then
        konsole --qwindowgeometry 700x200 -e youtube-dl --all-subs --skip-download "$url" 
        kdialog --msgbox " Download Complete "
        funcOtherSub
elif [ "$subs" = "d" ]
    then
        exit
fi
}

funcPlaylist() {
url=$(kdialog --title " YouTubeDownloader " --inputbox  " Choose an URL to Download " "URL")
path=$(kdialog --getexistingdirectory)
cd "$path" || exit
slist=$(kdialog --menu " Select an Option " a " Download Complete Playlist Video+Audio " b " Download Complete Playlist Audio " c " Download Playlist by Date " d " Download Playlist by Item Number " e " Exit Application " --geometry 400x200)
if [ "$slist" = "a" ]
    then
        funcVideo
elif [ "$slist" = "b" ]
    then
        funcAudio
elif [ "$slist" = "c" ]
    then
        funcDate
elif [ "$slist" = "d" ]
    then
        numb=$(kdialog --title " YouTubeDownloader " --inputbox " Choose Video Numbers to Download \ use 1,5,7,10 for expecific videos \ use 1-6 for expecific range \ use 1,3-5,4,6-8 for a combination of both " --geometry 300x300)
        qvideo=$(kdialog --title " YouTubeDownloader " --inputbox " Choose Resolution \ use 4320 2160 1440 1080 720 480 ")
        konsole --qwindowgeometry 700x200 -e youtube-dl -c -f bestvideo[height="$qvideo"]+bestaudio/best --playlist-items "$numb" "$url" 
        kdialog --msgbox " Download Complete "
        funcOtherPlaylist
elif [ "$slist" = "e" ]
    then
        exit
fi
}
funcDate() {
dmenu=$(kdialog --menu " Select an Option " a " Download Playlist Video+Audio by Date " b " Download Playlist Audio by Date " c " Exit Application ")
if [ "$dmenu" = "a" ]
    then
        date=$(kdialog --title " YouTubeDownloader " --inputbox " Choose date \ DATE is in the format YYYYMMDD as 20160507 \ use (now|today)[+-][0-9](day|week|month|year)(s) for expecific range as now-7days " --geometry 300x300)
        qvideo=$(kdialog --title " YouTubeDownloader " --inputbox " Choose Resolution \ use 1080 720 480 ")
        konsole --qwindowgeometry 700x200 -e youtube-dl -i -f bestvideo[height=$qvideo]+bestaudio/best --date "$date" "$url" 
        kdialog --msgbox " Download Complete"
        funcOtherDate
elif [ "$dmenu" = "b" ]
    then
        date=$(kdialog --title " YouTubeDownloader " --inputbox " Choose date \ DATE is in the format YYYYMMDD as 20160507 \ use (now|today)[+-][0-9](day|week|month|year)(s) for expecific range as now-7days " --geometry 300x300)
        abr=$(kdialog --title " Choose Audio Bitrate " --inputbox " Choose Bitrate between 0-9 \ best = 0 / worst = 9 ")
        konsole --qwindowgeometry 700x200 -e youtube-dl --extract-audio --audio-quality "$abr" --audio-format mp3 --date "$date" "$url" 
        kdialog --msgbox " Download Complete "
        funcOtherDate
elif [ "$dmenu" = "c" ]
    then
        exit
fi
}
        


funcOtherVideo() {
ovideo=$(kdialog --menu " Would you like to Download another Video" a " Sure Give It Up! " b " I'm Not Sure, Take Me To The Main Menu " c " What! Get The Hell Out Of Here! ") 
if [ "$ovideo" = "a" ]
    then
        funcVideo
elif [ "$ovideo" = "b" ]
    then
        funcMenu
elif [ "$ovideo" = "c" ]
    then
        exit
fi
}

funcOtherAudio() {
oaudio=$(kdialog --menu " Would you like to Download another Audio" a " Sure Give It Up! " b " I'm Not Sure, Take Me To The Main Menu " c " What! Get The Hell Out Of Here! ") 
if [ "$oaudio" = "a" ]
    then
        funcVideo
elif [ "$oaudio" = "b" ]
    then
        funcMenu
elif [ "$oaudio" = "c" ]
    then
        exit
fi
}

funcOtherSub() {
osub=$(kdialog --menu " Would you like to Download another Subtitle" a " Sure Give It Up! " b " I'm Not Sure, Take Me To The Main Menu " c " What! Get The Hell Out Of Here! ")  
if [ "$osub" = "a" ]
    then
        funcSubs
elif [ "$osub" = "b" ]
    then
        funcMenu
elif [ "$osub" = "c" ]
    then
        exit
fi
}

funcOtherDate() {
odate=$(kdialog --menu " Would you like to Download another Playlist by Date " a " Sure Give It Up! " b " I'm Not Sure, Take Me To The Main Menu " c " What! Get The Hell Out Of Here! ")  
if [ "$odate" = "a" ]
    then
        funcDate
elif [ "$odate" = "b" ]
    then
        funcMenu
elif [ "$odate" = "c" ]
    then
        exit
fi
}

funcprofile() {
kdialog --qwindowgeometry 700x500 --title " Info " --msgbox " If you always use the same or a couple options, this should make your life a litle bit easier. you'll be able to make your own option based on anyone of the default options in the application, even more, you can make your own option regardless all of the default options,save them and launch them from the profile menu. The profiles will be saved in ~/YouTubeDownloader/Profiles/ so you can easily edit,delete or whatever with them manually."
option=$(kdialog --title " Profile Menu " --menu " Select an option" a " Choose a Profile"  b " Make a Profile" c " Delete a Profile ")
if [ "$option" = "a" ]
    then
        file=$(kdialog --getopenfilename ~/YouTubeDownloader/Profiles/ "Profiles (*.sh)")
        konsole --qwindowgeometry 700x200 -e sh "$file"
        
elif [ "$option" = "b" ]
    then
        mkdir ~/YouTubeDownloader/Profiles/
        cd ~/YouTubeDownloader/Profiles/ || exit
        name=$(kdialog --title  " Make a Profile" --inputbox " Profile Name ---- don't use spaces in the name ")
        touch "${name}".sh && chmod +x 
        poption=$(kdialog --menu " Select an Option for the new Profile " a " Video Profile " b " Audio from Video Profile " c " Subtitles Profile " )
        if [ "$poption" = "a" ]
            then
                funcVideoProfile
        elif [ "$poption" = "b" ]
            then
                funcAudioProfile
        elif [ "$poption" = "c" ]
            then
                funcSubsProfile
       
        fi
elif [ "$option" = "c" ]
    then
        file=$(kdialog --getopenfilename ~/YouTubeDownloader/Profiles/ "Profiles (*.sh)")
        rm "$file"
fi
}

funcVideoProfile() {
        
        op=$(kdialog --title " ""$name"" Profile -YouTubeDownloader- " --inputbox " Choose Resolution for ""$name""  8K --> 4320p   4K --> 2160p   2K --> 1440p   FHD --> 1080p   HD --> 720p   SD --> 480p ----- use p.e. 4320 for 8K video ") 
            echo "
                #!/bin/bash
                url=$(kdialog --title " ${name} Profile -YouTubeDownloader- " --inputbox " Choose an URL to Download " "URL" --geometry 700x100)
                path=$(kdialog --getexistingdirectory)
                cd $path || exit
                konsole --qwindowgeometry 700x200 -e youtube-dl -f bestvideo[height=$op]+bestaudio/best ""$url""
                kdialog --msgbox " Download Complete " " >> "${name}".sh
        kdialog --title " ""$name"" Profile" --msgbox " Profile ""$name"" Created"
        funcMakeDesktopFile
            
}

funcAudioProfile() {

        format=$(kdialog --title " ""$name"" Profile -YouTubeDownloader- " --inputbox " Choose Audio Options for ""$name"" -------   mp3 aac wav opus ......")
        abr=$(kdialog --title " ""$name"" Profile -YouTubeDownloader- " --inputbox " Choose Bitrate between 0-9 ------   better <---- default 5 ----> worst ")
            echo "
                #!/bin/bash
                url=$(kdialog --title " ${name} Profile -YouTubeDownloader- " --inputbox " Choose an URL to Download " "URL" --geometry 700x100)
                path=$(kdialog --getexistingdirectory)
                cd $path || exit
                konsole --qwindowgeometry 700x200 -e youtube-dl --extract-audio --audio-quality ""$abr"" --audio-format ""$format"" ""$url"" 
                kdialog --msgbox " Download Complete " " >> "${name}".sh
        kdialog --title " ""$name"" Profile -YouTubeDownloader- " --msgbox " Profile Created"
        funcMakeDesktopFile

}

funcSubsProfile() {

        sf=$(kdialog --title " ${name} Profile -YouTubeDownloader- " --inputbox " Choose Subtitle Format \ ass    srt ")
        sl=$(kdialog --title " ${name} Profile -YouTubeDownloader- " --inputbox " Choose Subtitle language \ en  esp  pt  ita  ger ... ")
            echo "
                #!/bin/bash
                url=$(kdialog --title " ${name} Profile -YouTubeDownloader- " --inputbox " Choose an URL to Download " "URL" --geometry 700x100)
                path=$(kdialog --getexistingdirectory)
                cd $path || exit
                konsole --qwindowgeometry 700x200 -e youtube-dl --sub-format ""$sf"" --sub-lang ""$sl"" --skip-download ""$url"" 
                kdialog --msgbox " Download Complete " " >> "${name}".sh
            kdialog --title " ""$name"" Profile -YouTubeDownloader- " --msgbox " Profile Created"
            funcMakeDesktopFile
            
}

funcMakeDesktopFile() {

    desktop=$(kdialog --title " Make a *.desktop File" --yesno " Would you like to add to the applications menu launcher an option por this profile so you can launch this profile from there?")
    if [ "$desktop" = "0" ]
        then
            cd ~/.local/share/applications/ || exit
            touch "${name}".desktop && chmod +x "${name}".desktop
            echo "
                [Desktop Entry]
                Version=0.3
                Type=Application
                Exec=/home/$USER/YouTubeDownloader/Profiles/$name.sh
                Icon=YouTubeDownloader
                Name=""$name"".Profile.YouTubeDownloader
                Categories=Network;Utility" >> "${name}".desktop
            kdialog --title " Make a *.desktop File" --msgbox " ${name}.desktop Created "
    elif [ "$desktop" = "1" ]
        then
            exit
    fi
}



    
funcMenu



        





