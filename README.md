# YouTubeDownloader


   


1. Description:
    - This is a GUI for youtube-dl, a CLI program to download videos from different web sites.
    - YouTubeDownloader is able to download videos, extrac audio from a video, download subtitles, download youtube playlist,etc
  
 
2. Requeriments:
    1. youtube-dl
         - To install it:
            `sudo wget https://yt-dl.org/downloads/latest/youtube-dl -O /usr/local/bin/youtube-dl && sudo chmod a+rx /usr/local/bin/youtube-dl

         - To update it:
            `sudo youtube-dl -U

    2. ffmpeg
        1. Almost all distributions have it installed by default, if not:
            On Debian/Ubuntu based distributions
                sudo apt update && sudo apt full-upgrade && sudo apt install ffmpeg
            On KDE Neon
                sudo pkcon refresh && sudo pkcon update && sudo pkcon install ffmpeg
            On Arch based distributions
                sudo pacman -Syu && sudo pacman -S ffmpeg
    3. Kdialog
        1. Any distributions with KDE Plasma should have it installed, if not:
            On Debian/Ubuntu based distributions
                sudo apt update && sudo apt full-upgrade && sudo apt install kdialog
            On KDE Neon
                sudo pkcon refresh && sudo pkcon update && sudo pkcon install kdialog
            On Arch based distributions
                sudo pacman -Syu && sudo pacman -S kdialog
                
                
3. Instalation:
    1. Run install.sh as $USER with "sh install.sh" 
    2. This script will copy the files on their respective folders.
    3. Application folder YouTubeDownloader in your /home
    4. Launcher file *.desktop in /home/$USER/.local/share/applications/
    5. Icon file *.png in /home/$USER/.local/share/icons/
    6. The *.desktop file should be in that folder if you want to launch the application from your menu.
    7. If you want to move the application's folder or the icon file, just remember to edit the "EXEC=" and "ICON=" enty on *.desktop file with the new $PATH

4. Application Info and Use
    
    - Download Video
        1. If you use download a video by resolution 8k,4k,etc. It'll always gonna download the best quality Audio/Video available.
        2. If the best video option doesn't have audio, It'll download de best video option and the best audio option and merge them in an only file with Audio/Video tracks
        3. This option also works if you want to download a complete playlist
        4. Options:
            - Choose Quality option
                1. It'll analize the video and show you a list of all posible options to download:
                
                    - youtube-dl --list-formats https://www.youtube.com/watch?v=KpYaj-hsIkA
                    - [youtube] KpYaj-hsIkA: Downloading webpage
                    - [youtube] KpYaj-hsIkA: Downloading js player 8786a07b
                    - [info] Available formats for KpYaj-hsIkA:
                    - format code   extension   resolution  note
                    - 249           webm        audio only  tiny   67k , opus @ 50k (48000Hz), 2.47MiB
                    - 250           webm        audio only  tiny   82k , opus @ 70k (48000Hz), 3.24MiB
                    - 140           m4a         audio only  tiny  130k , m4a_dash container, mp4a.40.2@128k (44100Hz), 6.11MiB
                    - 251           webm        audio only  tiny  162k , opus @160k (48000Hz), 6.30MiB
                    - 394           mp4         256x144     144p   91k , av01.0.00M.08, 25fps, video only, 3.70MiB
                    - 160           mp4         256x144     144p  111k , avc1.4d400c, 25fps, video only, 4.36MiB
                    - 278           webm        256x144     144p  111k , webm container, vp9, 25fps, video only, 4.34MiB
                    - 395           mp4         426x240     240p  213k , av01.0.00M.08, 25fps, video only, 7.52MiB
                    - 242           webm        426x240     240p  225k , vp9, 25fps, video only, 8.58MiB
                    - 133           mp4         426x240     240p  299k , avc1.4d4015, 25fps, video only, 10.00MiB
                    - 396           mp4         640x360     360p  397k , av01.0.01M.08, 25fps, video only, 13.25MiB
                    - 243           webm        640x360     360p  405k , vp9, 25fps, video only, 14.68MiB
                    - 397           mp4         854x480     480p  670k , av01.0.04M.08, 25fps, video only, 21.86MiB
                    - 134           mp4         640x360     360p  678k , avc1.4d401e, 25fps, video only, 18.25MiB
                    - 244           webm        854x480     480p  759k , vp9, 25fps, video only, 21.71MiB
                    - 135           mp4         854x480     480p  906k , avc1.4d401e, 25fps, video only, 25.69MiB
                    - 136           mp4         1280x720    720p 1251k , avc1.4d401f, 25fps, video only, 34.35MiB
                    - 398           mp4         1280x720    720p 1357k , av01.0.05M.08, 25fps, video only, 42.41MiB
                    - 247           webm        1280x720    720p 1430k , vp9, 25fps, video only, 35.90MiB
                    - 399           mp4         1920x1080   1080p 2404k , av01.0.08M.08, 25fps, video only, 71.75MiB
                    - 248           webm        1920x1080   1080p 2652k , vp9, 25fps, video only, 94.17MiB
                    - 137           mp4         1920x1080   1080p 3858k , avc1.640028, 25fps, video only, 108.99MiB
                    - 18            mp4         640x360     360p  636k , avc1.42001E, 25fps, mp4a.40.2@ 96k (44100Hz), 30.03MiB (best)

                2. Note the format code number colum,that's the varialbe you need to put down in the next dialog to download the video you want.
                3. If you want to download several formats of the same video use a comma as a separator, e.g. 22,17,18 will download all these three formats 
            
    - Download Audio from Video
        1. This option will extract the audio track from any video
        2. Also works for a playlist
        3. Options:
            - Best Quality Mp3
                1. It'll extract/download the best quality audio option, If the choosen option isn't in mp3 format, It'll conver it to it
            - Choose Quality/Bitrate
                1. You can choose the audio quality of your file where 0 is the best quality, 5 is the normal quaity (128k) and 9 the worst quality.
            - Choose between Availables formats
                1. Just like the video option. See the video option for more information.
        
    - Download subtitles
        1. Options a and c are very self-explanatory
        2. Options
            - Subtitles by format/language
                - By Format
                    1. You can download the subtitles track in ass or srt format, e.g. ass for *.ass format
                - By Language
                    1. You can download the subtitles track in a expecified language. e.g. en for English
    - Download Playlist
        1. For options a and b see video or audio section.
        2. Options
            - Playlist by date
                1. Absolute dates: Dates in the format YYYYMMDD e.g. 20130414 (April 1 2019)
                2. Relative dates: Dates in the format (now|today)[+-][0-9](day|week|month|year)(s)? e.g. now-7days ( for the last 7 days videos)
            - Playlist by item number
                1. Specific items', use 1,5,9,10
                2. Specific items' range, use 1-5 or 1-5,9-15
                3. Mix items and range, use 1,3,5-8,9,10-13
                
    - Profiles
        1. It'll make a launcher for your profile with your specific options
        2. If you answer "yes" to make a *.desktop file, It'll add to the applications menu an option for this profile so you can launch it direcly.
        

5. Documentation:
    - [Youtube-dl GitHub repo](https://github.com/ytdl-org/youtube-dl/blob/master/)
    - [Ffmpeg Wiki](https://trac.ffmpeg.org/wiki)
    
            
    

  
